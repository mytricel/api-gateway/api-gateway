<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest


# API Gateway con gRPC

## Descripción

Esta API Gateway se encarga de recibir solicitudes HTTP desde el frontend para luego comunicarse con distintos microservicios utilizando el protocolo gRPC.

## Instalación de dependencias

A continuación se muestran las dependencias necesarias para la comunicación con los microservicios.

```bash
$ npm i @nestjs/microservices @grpc/grpc-js @grpc/proto-loader
$ npm i -D @types/node ts-proto
```

## Estructura del proyecto

Debe crearse una carpeta por cada microservicio.

```bash
$ nest g mo auth && nest g co auth --no-spec && nest g s auth --no-spec
$ nest g mo ceal && nest g co ceal --no-spec
$ nest g mo vote && nest g co vote --no-spec
$ touch src/auth/auth.guard.ts

```

## Scripts

Es necesario añadir scripts al archivo ```package.json``` para que se generen los archivos protobuf que se encuentran en el [Repositorio de protos compartidos](https://gitlab.com/mytricel/shared-protos/grpc-protos). Por lo que se añaden las siguientes líneas: (Es necesario tener [Protobuf](https://github.com/protocolbuffers/protobuf/releases) instalado y dejar el ```.exe``` en las variables de entorno)

```
"proto:install": "npm i git+https://gitlab.com/mytricel/shared-protos/grpc-protos.git",
"proto:auth": "protoc --plugin=node_modules/.bin/protoc-gen-ts_proto -I=./node_modules/shared-protos-mytricel/proto --ts_proto_out=src/auth/ node_modules/shared-protos-mytricel/proto/auth.proto --ts_proto_opt=nestJs=true --ts_proto_opt=fileSuffix=.pb",
"proto:ceal": "protoc --plugin=node_modules/.bin/protoc-gen-ts_proto -I=./node_modules/shared-protos-mytricel/proto --ts_proto_out=src/ceal/ node_modules/shared-protos-mytricel/proto/ceal.proto --ts_proto_opt=nestJs=true --ts_proto_opt=fileSuffix=.pb",
"proto:vote": "protoc --plugin=node_modules/.bin/protoc-gen-ts_proto -I=./node_modules/shared-protos-mytricel/proto --ts_proto_out=src/vote/ node_modules/shared-protos-mytricel/proto/vote.proto --ts_proto_opt=nestJs=true --ts_proto_opt=fileSuffix=.pb",
"proto:all": "npm run proto:auth && npm run proto:ceal && npm run proto:vote"
```

Luego dentro de la API se pueden ejecutar estos comandos:

```
$ npm run proto:install && npm run proto:all
```

# Run

```bash
# watch mode
$ npm run start:dev

```

## Test

### Instalación de dependencias
```bash
$ npm install --save-dev jest @types/jest ts-jest @nestjs/testing
```

### Configuracion de Archivo Jest

Se agrega este archivo en la raiz del proyecto.

```bash
module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: [
    '**/*.(t|j)s',
  ],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
};
```

```bash
# test coverage
$ npm run test:cov
```