import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { CealModule } from './ceal/ceal.module';
import { VoteModule } from './vote/vote.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    AuthModule, 
    CealModule, 
    VoteModule
  ],  
})
export class AppModule {}
