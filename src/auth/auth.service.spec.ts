import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { ClientsModule, Transport, ClientGrpcProxy } from '@nestjs/microservices';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AUTH_SERVICE_NAME, AUTH_PACKAGE_NAME, ValidateResponse, AuthServiceClient } from './auth.pb';
import { of } from 'rxjs';

describe('AuthService', () => {
  let service: AuthService;
  let clientGrpc: ClientGrpcProxy;
  let authServiceClient: { validate: jest.Mock };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        ClientsModule.registerAsync([
          {
            name: AUTH_SERVICE_NAME,
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
              transport: Transport.GRPC,
              options: {
                url: configService.get<string>('MS_AUTH_URL'),
                package: AUTH_PACKAGE_NAME,
                protoPath: 'node_modules/shared-protos-mytricel/proto/auth.proto',
              },
            }),
            inject: [ConfigService],
          },
        ]),
      ],
      providers: [AuthService],
    }).compile();

    service = module.get<AuthService>(AuthService);
    clientGrpc = module.get<ClientGrpcProxy>(AUTH_SERVICE_NAME);

    authServiceClient = {
      validate: jest.fn(),
    };

    jest.spyOn(clientGrpc, 'getService').mockReturnValue(authServiceClient);

    service.onModuleInit();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('validate', () => {
    it('should call authServiceClient.validate and return the result', async () => {
      const token = 'valid-token';
      const validateResponse: ValidateResponse = {
        status: 200,
        userId: 1,
        error: [],
      };

      authServiceClient.validate.mockReturnValue(of(validateResponse));

      const result = await service.validate(token);

      expect(result).toEqual(validateResponse);
      expect(authServiceClient.validate).toHaveBeenCalledWith({ token });
    });
  });
});
