import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { ClientsModule, Transport, ClientGrpcProxy } from '@nestjs/microservices';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AUTH_SERVICE_NAME, AUTH_PACKAGE_NAME, RegisterRequest, RegisterResponse, LoginRequest, LoginResponse, AuthServiceClient } from './auth.pb';
import { of } from 'rxjs';

describe('AuthController', () => {
  let controller: AuthController;
  let authServiceClient: { register: jest.Mock; login: jest.Mock };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        ClientsModule.registerAsync([
          {
            name: AUTH_SERVICE_NAME,
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
              transport: Transport.GRPC,
              options: {
                url: configService.get<string>('MS_AUTH_URL'),
                package: AUTH_PACKAGE_NAME,
                protoPath: 'node_modules/shared-protos-mytricel/proto/auth.proto',
              },
            }),
            inject: [ConfigService],
          },
        ]),
      ],
      controllers: [AuthController],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authServiceClient = {
      register: jest.fn(),
      login: jest.fn(),
    };

    // Simulate the ClientGrpcProxy instance to return our mock
    const clientGrpcProxy = module.get<ClientGrpcProxy>(AUTH_SERVICE_NAME);
    jest.spyOn(clientGrpcProxy, 'getService').mockReturnValue(authServiceClient);

    // Initialize the controller to set the service client
    controller.onModuleInit();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register', () => {
    it('should call authServiceClient.register and return the result', async () => {
      const registerRequest: RegisterRequest = {
        email: 'test@example.com',
        password: 'password',
        role: 'user'
      };
      const registerResponse: RegisterResponse = {
        status: 200,
        error: []
      };

      authServiceClient.register.mockReturnValue(of(registerResponse));

      const result = await controller.register(registerRequest);
      result.subscribe(response => {
        expect(response).toEqual(registerResponse);
      });

      expect(authServiceClient.register).toHaveBeenCalledWith(registerRequest);
    });
  });

  describe('login', () => {
    it('should call authServiceClient.login and return the result', async () => {
      const loginRequest: LoginRequest = {
        email: 'test@example.com',
        password: 'password'
      };
      const loginResponse: LoginResponse = {
        status: 200,
        error: [],
        token: 'some-token'
      };

      authServiceClient.login.mockReturnValue(of(loginResponse));

      const result = await controller.login(loginRequest);
      result.subscribe(response => {
        expect(response).toEqual(loginResponse);
      });

      expect(authServiceClient.login).toHaveBeenCalledWith(loginRequest);
    });
  });
});
