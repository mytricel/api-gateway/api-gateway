import { Test, TestingModule } from '@nestjs/testing';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { ExecutionContext, HttpStatus, UnauthorizedException } from '@nestjs/common';
import { ValidateResponse } from './auth.pb';
import { Request } from 'express';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthGuard,
        {
          provide: AuthService,
          useValue: {
            validate: jest.fn(),
          },
        },
      ],
    }).compile();

    guard = module.get<AuthGuard>(AuthGuard);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(guard).toBeDefined();
  });

  describe('canActivate', () => {
    it('should return true if token is valid', async () => {
      const mockRequest = {
        headers: {
          authorization: 'Bearer valid-token',
        },
      } as unknown as Request;

      const mockExecutionContext = {
        switchToHttp: jest.fn().mockReturnValue({
          getRequest: jest.fn().mockReturnValue(mockRequest),
        }),
      } as unknown as ExecutionContext;

      const validateResponse: ValidateResponse = {
        status: HttpStatus.OK,
        userId: 1,
        error: [],
      };

      (authService.validate as jest.Mock).mockResolvedValue(validateResponse);

      const result = await guard.canActivate(mockExecutionContext);
      expect(result).toBe(true);
      expect(authService.validate).toHaveBeenCalledWith('valid-token');
    });

    it('should throw UnauthorizedException if no authorization header', async () => {
      const mockRequest = {
        headers: {},
      } as unknown as Request;

      const mockExecutionContext = {
        switchToHttp: jest.fn().mockReturnValue({
          getRequest: jest.fn().mockReturnValue(mockRequest),
        }),
      } as unknown as ExecutionContext;

      await expect(guard.canActivate(mockExecutionContext)).rejects.toThrow(UnauthorizedException);
    });

    it('should throw UnauthorizedException if authorization header is malformed', async () => {
      const mockRequest = {
        headers: {
          authorization: 'Bearer',
        },
      } as unknown as Request;

      const mockExecutionContext = {
        switchToHttp: jest.fn().mockReturnValue({
          getRequest: jest.fn().mockReturnValue(mockRequest),
        }),
      } as unknown as ExecutionContext;

      await expect(guard.canActivate(mockExecutionContext)).rejects.toThrow(UnauthorizedException);
    });

    it('should throw UnauthorizedException if token is invalid', async () => {
      const mockRequest = {
        headers: {
          authorization: 'Bearer invalid-token',
        },
      } as unknown as Request;

      const mockExecutionContext = {
        switchToHttp: jest.fn().mockReturnValue({
          getRequest: jest.fn().mockReturnValue(mockRequest),
        }),
      } as unknown as ExecutionContext;

      const validateResponse: ValidateResponse = {
        status: HttpStatus.UNAUTHORIZED,
        userId: null,
        error: ['Invalid token'],
      };

      (authService.validate as jest.Mock).mockResolvedValue(validateResponse);

      await expect(guard.canActivate(mockExecutionContext)).rejects.toThrow(UnauthorizedException);
    });
  });
});
