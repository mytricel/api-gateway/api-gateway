import { Module } from '@nestjs/common';
import { CealController } from './ceal.controller';

@Module({
  controllers: [CealController]
})
export class CealModule {}
